<?php
include "utils.php";
include "cors.php";
include "mail.php";
$name = testInput($_POST["name"]);
$surname = testInput($_POST["surname"]);
$email = testInput($_POST["email"]);
$password = testInput($_POST["password"]);

$conn = openConnection();
$query = "SELECT Id FROM User WHERE Email LIKE ?";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$email);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows == 0) {
    $activationToken = uniqid();
    $random_salt = hash('sha256', uniqid(mt_rand(1, mt_getrandmax()), true));
    $hashedPassword = hash('sha256', $password.$random_salt);
    $query = "INSERT INTO User(Name, Surname, Email, HashedPassword, ActivationToken, Salt) VALUES(?,?,?,?,?,?)";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('ssssss', $name, $surname, $email, $hashedPassword, $activationToken, $random_salt);
    $stmt->execute();
    $conn->close();
    sendMail($email, $activationToken);
    http_response_code(200);
} else {
    http_response_code(403);
}
?>