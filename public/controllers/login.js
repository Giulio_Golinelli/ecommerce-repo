import { isLogged, setUser, getUser, eraseUser } from "/controllers/user.js"
import { setShoppingBag } from "/controllers/shoppingBag.js"

$(function() {
    $('.alert').hide()
    //On form submit check for correct credentials and store user information retrieved from backend
    $("#myForm").submit(function(e) {
        e.preventDefault();
        let hashedPassword = CryptoJS.SHA256($("#password").val())
        $.ajax({
            url: "/backend/login.php",
            type: 'POST',
            data: jQuery.param({
                email: $("#email").val(),
                password: hashedPassword.toString(CryptoJS.enc.Hex),
            }),
            success: () => {
                $.ajax({
                    url: "/backend/account.php",
                    type: "GET",
                    success: (data) => {
                        setUser(data)
                        setShoppingBag([])
                        window.location.replace("index.html")
                    }
                })
            },
            error: function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status == 403){
                    $('#alert').empty();
                    $('#alert').append('<div class="alert alert-danger" role="alert">'+jqXHR.responseText+'</div>')
                }
            }
        });
    });

});