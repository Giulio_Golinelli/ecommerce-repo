<?php
include "cors.php";
include "utils.php";
$token = validateToken();
$conn = openConnection();
$userId = getUserId($token);
$query = "SELECT Name, DATE_FORMAT(Date, '%m/%d/%Y %H:%i') as Date FROM Messages WHERE UserId=? ORDER BY Date DESC";
$stmt = $conn->prepare($query);
$stmt->bind_param('i',$userId);
$stmt->execute();
$result = $stmt->get_result();
$conn->close();
$messages = array();
while($row = $result->fetch_assoc()) {
    $message = array();
    $message["Name"] = $row["Name"];
    $message["Datetime"] = $row["Date"];
    array_push($messages,$message);
}
echo json_encode($messages);
?>
