# ECOMMERCE
*Made by Giulio Golinelli, Filippo Cavallari and Lorenzo Balzani*

Ecommerce is an ecommerce site where every user can sell and buy products!

# Features
- Responsive 📲
- Reactive (100% AJAX driven) 🧙‍♂️
- Easy to use and intuitive UI/UX 👨‍🔬
- Stateful ⏰
- MVC based 🛡️
- Secure 🔒
- Hosted (http://37.187.122.91/) 🏠

# Changelog
- 06/01/2020: Ecommerce v.: 1.0

# Credits
- Filippo Cavallari: filippo.cavallari2@studio.unibo.it
- Giulio Golinelli: giulio.golinelli@studio.unibo.it
- Lorenzo Balzani: lorenzo.balzani@studio.unibo.it

