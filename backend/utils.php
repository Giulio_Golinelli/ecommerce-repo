<?php
function generateToken($userId)
{
    $token = uniqid();
    $conn = openConnection();
    $query = "INSERT INTO Token(Token, UserId) VALUES (?,?)";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('si',$token,$userId);
    $stmt->execute();
    setcookie("token", $token, time() + (86400 * 30), "/");
    $conn->close();
    return $token;
}

function openConnection()
{
    $servername = "localhost";
    $username = "sec_user";
    $password = "gMWBCgMsUmFGQJZrdavNzJE9LUjSV2m8";
    $connection = new mysqli($servername, $username, $password, "Unibo");
    if ($connection->connect_error) {
        echo "Failed to connect to MySQL: " . $connection->connect_error;
        exit();
    } else {
        return $connection;
    }
}

function getUserId($token): int{
    $conn = openConnection();
    $stmt = $conn->prepare("SELECT UserId FROM Token WHERE Token LIKE ?");
    $stmt->bind_param('s',$token);
    $stmt->execute();
    $id = $stmt->get_result()->fetch_assoc()["UserId"];
    $conn->close();
    return $id;
}

function validateToken()
{
    if (!isset($_COOKIE['token'])) {
        echo "Token not valid!";
        http_response_code(403);
        exit();
    }
    $token = testInput($_COOKIE['token']);
    $conn = openConnection();
    $query = "SELECT * FROM Token WHERE Token LIKE ?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('s',$token);
    $stmt->execute();
    if ($result = $stmt->get_result()) {
        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            if (strtotime($row['Timestamp']) + 3600 < strtotime("now")) {
                return updateToken($row['UserId']);
            } else {
                return $token;
            }
        } else {
            echo "Token not valid!";
            http_response_code(403);
            exit();
        }
    } else {
        echo "Query Token Failed!";
        http_response_code(500);
        exit();
    }
}

function deleteTokenById($userId)
{
    $conn = openConnection();
    $query = "DELETE FROM Token WHERE UserId=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("i",$userId);
    $stmt->execute();
    $conn->close();
}

function updateToken($userId)
{
    deleteTokenById($userId);
    return generateToken($userId);
}

function testInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
