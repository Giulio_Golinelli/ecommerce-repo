<?php
include "utils.php";
include "cors.php";

$email = testInput($_POST["email"]);
$password = testInput($_POST["password"]);

$conn = openConnection();
$query="SELECT * FROM User WHERE Email = ? LIMIT 1";
$stmt = $conn->prepare($query);
$stmt->bind_param('s', $email);
$stmt->execute();
if ($result = $stmt->get_result()) {
    if($result->num_rows == 1) {
        $row = $result -> fetch_assoc();
        if(!$row["Activated"]){
            echo "You must activate your account first";
            http_response_code(403);
            return;
        }
        $hashedPassword = hash('sha256', $password.$row["Salt"]);
        if($row["HashedPassword"] == $hashedPassword) {
            $userId = $row['Id'];
            $token = generateToken($userId);
            //setcookie("token", $token, time() + (86400 * 30), "/");
            http_response_code(200);
        } else {
            echo "Wrong password!";
            http_response_code(403);
        }
    } else {
        echo "Wrong Email!";
        http_response_code(403);
    }
}
$conn->close();
?>