// include user/shopping bag management script
import { isLogged, getUser, eraseUser } from "/controllers/user.js"
import { setShoppingBag, getShoppingBag, eraseShoppingBag, insertTo, deleteFrom } from "/controllers/shoppingBag.js"

let originalProducts = [];
let products = [];
let messages = [];
let newMessages;

const insertIntoShoppingBag = (productId, quantity) => {
    let product = originalProducts.find(p => p.Id === productId)
    insertTo(product, quantity)
}

const deleteFromShoppingBag = (productId) => {
    deleteFrom(productId)
}

const fillShoppingBag = () => {
    $('#cartModalBody').empty()
    if(getShoppingBag().length == 0)
        $("#cartModalBody").append('<div class="d-flex justify-content-center"><span class="rt-small font-italic">Here you will find all your items added to she shooping cart! :D</span></div>')
    else {
        let total = 0;
        getShoppingBag().forEach(function (o) {
            total += o.product.Price*o.quantity
            let order = `
            <div class="d-flex justify-content-between align-items-center border border-secondary rounded all-c">
                <div id="cartProduct${o.product.Id}" class="d-flex flex-column rt">
                    <div>
                        <span class="font-weight-bold">#${o.product.Id}</span>
                        <span>${o.product.Name}</span>
                    </div>
                    <span>Quantity: ${o.quantity}</span>
                </div>
                <div>
                    <span class="font-weight-bold rt-big">${o.product.Price*o.quantity} $</span>
                    <i class="fas fa-times interactive deleteProduct" data-product="${o.product.Id}"></i>
                </div>
            </div>`
            $("#cartModalBody").append(order)
        })
        $("#cartModalBody").append(`
            <div class="d-flex justify-content-between align-items-center all-c">
                <span class="rt">Totale: ${total} $</span>
                <button id="buyCart" type="button" class="btn btn-success">Buy!</button>
            </div>`)
    }
}

const buy = () => {
    $("#cartModal").modal('hide')
    $("#loadingModal").modal('show')
    setTimeout(() => {
        $('#loadingModal').modal('hide');
        getShoppingBag().map(item => {
            $.ajax({
                url: "/backend/buy.php",
                type: 'POST',
                data: jQuery.param({
                    productId: item.product.Id,
                    quantity: item.quantity,
                }),
                success: () => {
                    reloadProducts()
                    setShoppingBag([])
                    fetchMessages()
                },
                error: (data) => console.log(data),
            })
        })
    }, 5000);
}

function buildMessages() {
    let content = ''
    if (messages.length > 0)
        messages.forEach((m) => {
            content += '<div class="row"><div class="col pl-0 pr-0 hover"><div class="row"><div class="col text-left rt-small mr-1 ml-1">' + m.Name + '</div></div><div class="row"><div class="col text-left mr-1 ml-1">' + m.Datetime + '</div></div></div></div>'
        })
    else
        content += '<div class="row"><div class="col pl-0 pr-0">There are no messages</div></div>'
    return content
}

function fetchMessages() {
    $.ajax({
        url: "/backend/getMessages.php",
        type: 'GET',
        success: (data) => {
            mergeMessages(JSON.parse(data))
        },
        error: (data) => console.log(data),
    })
}

function mergeMessages(newMessages) {
    if(messages.length != 0){
        //difference only in the new array cause it is a superset of the old one
        let difference = newMessages.filter(m1 => !messages.some(m2 => m2.Name == m1.Name));
        if(difference.length > 0){
            $("#messagesIcon").addClass("text-success")
            difference.forEach(m => {
                newToast(m.Name)
            })
        }
    }
    messages = newMessages
}

//updates visually products
function fillProducts () {
    $("#myProducts").empty();
    $("#main").empty()
    products.forEach(function (p) {
        let owned = isLogged() && p.Seller == getUser().userId
        let productHTML = `
            <form id="${p.Id}buyForm" data-product="${p.Id}">
                <div class="d-flex justify-content-around align-items-center border border-secondary rounded mt-2 mb-2 p-2">
                <img class="productImage" src="../images/${p.Image}" alt="${p.Name}" class="rt"></img>
                    <div class="d-flex flex-column">
                        <span class="rt">${p.Name}</span>`
        if(!owned)
            productHTML += `<span class="rt-small">Sold by: ${p.SellerName}</span>`
        productHTML += `
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <span class="rt-small">${p.Quantity}</span>
                        <span class="rt-small">Only available!</span>
                    </div>
                    <span class="rt font-weight-bold">${p.Price} $</span>`
        //if is logged show buy button
        if(isLogged() && !owned)
            productHTML += `
                <div class="d-flex flex-column align-items-center">
                    <div>
                        <button type="submit" name="${p.Id}" class="buyButton transparent-input"><i class="fas fa-cart-plus interactive rt-big"></i></button>
                    </div>    
                    <div>
                        <label for="buyQuantity${p.Id}" class="mr-2">Quantity:</label>
                        <input name="quantity" id="buyQuantity${p.Id}" type="number" class="transparent-input input-resize" value="1" min="1">
                    </div>
                </div>`
        else if (!isLogged())
            productHTML += '<span class="font-italic rt-small">LogIn to buy this product!</span>'
        productHTML += "</div></form>"
        owned ? $("#myProducts").append(productHTML) : $("#main").append(productHTML)
    })
    let noProducts = '<div class="d-flex justify-content-center"><span class="rt-small font-italic">No items Found! :(</span></div>'
    let logInToSee = '<div class="d-flex justify-content-center"><span class="rt-small font-italic">LogIn to see your products!</span></div>'
    if(isLogged()){
        //if there aren't products sold by logged account
        if(!products.some(p => p.Seller == getUser().userId))
            $("#myProducts").append(noProducts)
        //if there aren't  products sold by someone else
        if(!products.some(p => p.Seller != getUser().userId))
            $("#main").append(noProducts)
    }
    else {
        $("#myProducts").append(logInToSee)
        if(products.length == 0)
            $("#main").append(noProducts)
    }
    $("input[type=number].input-resize").each(resizeInput) //reload inputs
}

const filterProducts = async (searchKey) => {
    if (searchKey === "") {
        products = originalProducts
    } else {
        products = [];
        originalProducts.map(product => {
            if (product.Name.toLowerCase().includes(searchKey)) products.push(product)
        })
    }
}

//updates logically products
async function getProducts(){
    await $.ajax({
        url: "/backend/getProducts.php",
        type: 'GET',
        success: (data) => {
            products = JSON.parse(data);
            originalProducts = products;
        }
    })
}

//updates logically products via getProducts and once that's ended,
//updates visually products via fillProducts
function reloadProducts() {
    getProducts().then(() => fillProducts())
}

const sortByLowestPrice = (a, b) => {
    let priceA = parseInt(a.Price, 10);
    let priceB = parseInt(b.Price, 10);
    return ((priceA < priceB) ? -1 : ((priceA > priceB) ? 1 : 0));
}

const sortByHighestPrice = (a, b) => {
    let priceA = parseInt(a.Price, 10);
    let priceB = parseInt(b.Price, 10);
    return ((priceA > priceB) ? -1 : ((priceA > priceB) ? 1 : 0));
}

const sortNewest = (a, b) => {
    return ((b.DateAdded < a.DateAdded) ? -1 : ((b.DateAdded > a.DateAdded) ? 1 : 0));
}

function newToast(message){
    $("#toastBody").empty()
    $("#toastBody").append(message)
    $("#toast").toast("show")
}

$(() => {
    reloadProducts()
    setInterval(reloadProducts, 60000) //every miunte
    //conditional logic
    if(isLogged()) {
        fetchMessages()
        setInterval(fetchMessages, 2000) //every 5 seconds
        $("#signInBox").append(
            `<i class="fas fa-shopping-cart interactive" data-toggle="modal" data-target="#cartModal"></i>
            <a href="profile.html" class="interactive"><i class="fas fa-user-circle"></i></a>
            <i id="logOutButton" class="interactive fas fa-sign-out-alt"></i>`);
        $("#addProductBox").addClass("d-flex")
        $("#addProductBox").show()
        $("#messagesIcon").popover({
            animation: true, 
            container: "body",
            placement: "bottom",
            title: "Messages",
            html: true,
            content: buildMessages(),
        })
        $("#toast").toast({delay: 3500})
    } else {
        $("#messagesIcon").hide()
        $("#signInBox").append('<button id="signUpButton" type="button" class="btn btn-outline-primary btn-sm">Sign Up</button> <button id="logInButton" type="button" class="btn btn-primary btn-sm">Log In</button>')
        $("#addProductBox").removeClass("d-flex")
        $("#addProductBox").hide()
    }
    $('#addProductForm').on('submit', function(e) {
        e.preventDefault();
        let formData = new FormData(this);
        $.ajax({
            url: "/backend/addProduct.php",
            type: 'POST',
            data: formData,
            //data: $("#addProductForm").serialize(),
            success: () => reloadProducts(),
            cache: false,
            contentType: false,
            processData: false
        })
    })
   
    $("#selectOrder").on("change", () => {
        switch ($('#selectOrder :selected').val()) {
            case "lowestPrice":
                products.sort(sortByLowestPrice);
                break;
            case "highestPrice":
                products.sort(sortByHighestPrice);
                break;
            case "newest":
                products.sort(sortNewest);
                break;
        }
        fillProducts();
    });

    $("#signInBox").on("click", "#logInButton", () => redirect("login.html"))
    $("#signInBox").on("click", "#signUpButton", () => redirect("signup.html"))
    $("#signInBox").on("click", "#logOutButton", () => {
        eraseUser()
        eraseShoppingBag()
        document.cookie = 'token=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        location.reload(); 
    })

    $("#main").on("submit", "form", (e) => {
        e.preventDefault()
        newToast("Hai aggiunto un ordine al carrello")
        let productId = $("#"+e.currentTarget.id).attr("data-product")
        insertIntoShoppingBag(productId, $("#buyQuantity"+productId).val())
    })

    $('#imageInputButton').click(() => $("#imageInput").click())
    $('#searchBar').keyup((e) => filterProducts(e.currentTarget.value.toLowerCase()).then(() => fillProducts()))

    $("#messagesIcon").on("inserted.bs.popover", function () {
        $("#messagesIcon").removeClass("text-success")
        $("#messagesBox").append(buildMessages())
    })

    //Fires when the modal is opening
    $('#cartModal').on('show.bs.modal', () => fillShoppingBag())

    $("#cartModalBody").on("click", ".deleteProduct", (p) => {
        deleteFromShoppingBag(p.target.getAttribute("data-product"))
        fillShoppingBag()
    })
    $("#cartModalBody").on("click", "#buyCart", () => buy())
});
