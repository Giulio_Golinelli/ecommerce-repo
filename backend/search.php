<?php
include "cors.php";
$keyword = '%'.testInput($_GET["keyword"]).'%';
$conn = openConnection();

$query = "SELECT * FROM Product WHERE Quantity > 0 AND Name LIKE ?";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$keyword);
$stmt->execute();
$result = $stmt->get_result();
$products = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $product = array();
        $product["Id"] = $row["Id"];
        $product["Name"] = $row["Name"];
        $product["Description"] = $row["Description"];
        $product["Price"] = $row["Price"];
        $product["Quantity"] = $row["Quantity"];
        array_push($products, $product);
    }
}
echo json_encode($products);
http_response_code(200);
?>
