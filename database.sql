create table User
(
    Id             int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name           varchar(30),
    Surname        varchar(30),
    Email          varchar(50),
    HashedPassword varchar(50)
);

create table Token(
    Token varchar(255) NOT NULL UNIQUE PRIMARY KEY,
    Timestamp timestamp default current_timestamp,
    UserId int NOT NULL,
    FOREIGN KEY (UserId) REFERENCES User(Id)
);

create table Product(
    Id int AUTO_INCREMENT PRIMARY KEY,
    Name varchar(255),
    Description varchar(255),
    Price double,
    Quantity int CHECK (Quantity >= 0),
    Seller int,
    FOREIGN KEY (Seller) REFERENCES User(Id)
);

create table Purchase(
    Id int AUTO_INCREMENT PRIMARY KEY,
    Price double NOT NULL,
    Quantity int default 1,
    Timestamp timestamp  default  current_timestamp,
    Product int NOT NULL,
    Customer int NOT NULL,
    FOREIGN KEY (Product) REFERENCES Product(Id),
    FOREIGN KEY (Customer) REFERENCES User(Id)
);

CREATE TABLE Messages(
                         Id INT AUTO_INCREMENT PRIMARY KEY,
                         Name varchar(255) NOT NULL,
                         Date DATETIME DEFAULT NOW(),
                         UserId INT NOT NULL,
                         FOREIGN KEY (UserId) REFERENCES User(Id)
);