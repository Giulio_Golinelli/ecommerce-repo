<?php
include "cors.php";
include "utils.php";
$token = validateToken();
$conn = openConnection();
$query = "SELECT DISTINCT Purchase.Id, Product.Name, Product.Description, Product.Image, Purchase.Price, Purchase.Quantity, Purchase.Timestamp, Purchase.Finished, Purchase.Status, User.Name as UserName
    FROM Product, Purchase, User WHERE Purchase.Customer = User.Id AND Product.Id = Purchase.Product AND Seller=(SELECT UserId FROM Token WHERE Token LIKE ?) ORDER BY Purchase.Timestamp DESC";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$token);
$stmt->execute();
$result = $stmt->get_result();
$conn->close();
$products = array();
while ($row = $result->fetch_assoc()) {
    $product = array();
    $product["Id"] = $row["Id"];
    $product["Name"] = $row["Name"];
    $product["Description"] = $row["Description"];
    $product["Price"] = $row["Price"];
    $product["Quantity"] = $row["Quantity"];
    $product["DateAdded"] = $row["DateAdded"];
    $product["Finished"] = $row["Finished"];
    $product["Status"] = $row["Status"];
    $product["Customer"] = $row["UserName"];
    $product["Image"] = $row["Image"];
    array_push($products, $product);
}
echo json_encode($products);
http_response_code(200);
?>