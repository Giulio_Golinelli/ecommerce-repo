<?php
include "cors.php";
include "utils.php";
$token = validateToken();
$conn = openConnection();
$productId = testInput($_POST['productId']);
$name = testInput($_POST['name']);
$description = testInput($_POST['description']);
$price = testInput($_POST['price']);
$quantity = testInput($_POST['quantity']);
$userId = getUserId($token);
$query = "SELECT Id FROM Product WHERE Seller=? AND Id=?";
$stmt = $conn->prepare($query);
$stmt->bind_param('ii',$userId,$productId);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $query = "UPDATE Product SET Name=?,Description=?,Price=?,Quantity=? WHERE Id=?";
    $stmt = $conn->prepare($query);
    $stmt->prepare('ssdii', $name, $description, $price, $quantity, $productId);
    $stmt->execute();
    http_response_code(200);
} else {
    echo "You can't update a product that you are not selling!";
    http_response_code(403);
}
$conn->close();
?>