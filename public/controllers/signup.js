$(function() {
    $('.alert').hide()
    //On form submit check for correct credentials and store user information retrieved from backend
    $("#signUpForm").submit(function(e) {
        e.preventDefault();
        let hashedPassword = CryptoJS.SHA256($("#password").val())
        $.ajax({
            url: "/backend/register.php",
            type: 'POST',
            data: jQuery.param({
                name: $("#name").val(),
                surname: $("#surname").val(),
                email: $("#email").val(),
                password: hashedPassword.toString(CryptoJS.enc.Hex),
            }),
            success: () => {
                $('#alert').append(`
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Well done!</h4>
                        <p>Check your email inbox and verify your account!</p>
                  </div>`)
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);
            }
        });
    });

});