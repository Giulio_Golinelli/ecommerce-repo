<?php
include "utils.php";

$uuid = testInput($_GET["uuid"]);
$conn = openConnection();
$query = "SELECT Id, ActivationToken FROM User WHERE ActivationToken LIKE ?";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$uuid);
$stmt->execute();
$result = $stmt->get_result();
if($result->num_rows == 1){
    $row = $result->fetch_assoc();
    $userId = $row["Id"];
    $query = "UPDATE User SET Activated=TRUE WHERE Id=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('i',$userId);
    $stmt->execute();
    echo "Account successfully activated</br>You will be redirected in 5 seconds...";
    header("Refresh:5; url=http://37.187.122.91/login.html");
    http_response_code(200);
}else{
    echo 'Invalid activation code';
    http_response_code(403);
}
$conn->close();
?>
