
//Returns true if the viewport is small else return false
function checkViewportSize(){
    return $(window).width() < 768;
}

//INPUT:    A valid id for a DOM element
//          A valid class
//This function switch on/off a class of a DOM element based on a boolean function
function switchCssClass(id, css_class, f){
    //check if element exist
    if($('#'+id).length)
        //switch on/off the class based on return of f
        if(f())
            $('#'+id).addClass(css_class)
        else
            $('#'+id).removeClass(css_class)   
}

//compute a redirect based on current folder path
function redirect(path){
    let loc = window.location.pathname;
    window.location.href = loc.substring(0, loc.lastIndexOf('/'))+'/'+path
}

//resize width of input type number element based on its content
function resizeInput() {
    $(this).css("width", Math.max(1, $(this).val().length+1)+"ch")
    if($(this).val().length == 0)
        $(this).addClass("border border-secondary rounded")
    else
        $(this).removeClass("border border-secondary rounded")
}

//Executed when DOM is ready to be manipulated
$(function() {
    $("input[type=number].input-resize").each(resizeInput) //updates every input 1 time
    $("body").on("keyup", "input[type=number].input-resize", resizeInput) //every static and dynamic input changes
});