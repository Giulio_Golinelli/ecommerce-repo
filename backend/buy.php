<?php
include "cors.php";
include "utils.php";

$token = validateToken();
$conn = openConnection();
$productId = testInput($_POST['productId']);
$quantity = testInput($_POST['quantity']);
$query = "SELECT Quantity, Price, Seller, Name FROM Product WHERE Id=?";
$stmt = $conn->prepare($query);
$stmt->bind_param('i',$productId);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $dbQuantity = $row['Quantity'];
    $newQuantity = $dbQuantity - $quantity;
    if ($newQuantity < 0) {
        http_response_code(403);
        return;
    }
    $price = (double)($row['Price'] * $quantity);
    $seller = $row['Seller'];
    $name = $row['Name'];
    $query = "UPDATE Product SET Quantity=? WHERE Id=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('ii',$newQuantity,$productId);
    $stmt->execute();
    $userId = getUserId($token);
    $query = "INSERT INTO Purchase(Price,Quantity,Product,Customer) VALUES(?,?,?,?)";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('diii',$price,$quantity,$productId,$userId);
    $stmt->execute();
    $query = "SELECT Id FROM Purchase WHERE Price=? AND Quantity=? AND Product=? AND Customer=? ORDER BY Timestamp DESC";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('diii',$price,$quantity,$productId,$userId);
    $stmt->execute();
    $row = $stmt->get_result()->fetch_assoc();
    $orderId = $row["Id"];
    $message = 'Your order #'. $orderId . ' has been accepted';
    $query = "INSERT INTO Messages(Name, UserId) VALUES(?,?)";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('si',$message,$userId);
    $stmt->execute();
    $message = 'You have received the order #' . $orderId . ' of '. $quantity . ' ' . $name . ' for a total of ' .  $price . '$';
    $stmt = $conn->prepare("INSERT INTO Messages(Name, UserId) VALUES(?,?)");
    $stmt->bind_param('si',$message,$seller);
    $stmt->execute();
    http_response_code(200);
} else {
    http_response_code(403);
}
?>