import { isLogged, getUser} from "/controllers/user.js"

$(() => {
    let purchasedItems = [];
    let soldItems = [];
    let noPrdocts = '<div class="d-flex justify-content-center"><span class="rt-small font-italic">No items Found! :(</span></div>'

    const getOrders = () => {
        $.ajax({
            url: "/backend/orders.php",
            type: 'POST',
            success: (data) => {
                if (data) {
                    purchasedItems = JSON.parse(data);
                    $("#nItemsPurchased").html(purchasedItems.length);
                    showInProgressPurchases();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);
            }
        })
    }

    const getSoldItems = () => {
        $.ajax({
            url: "/backend/getSoldProducts.php",
            type: 'POST',
            success: (data) => {
                if (data) {
                    soldItems = JSON.parse(data);
                    $("#nItemsSold").html(soldItems.length);
                    showInProgressSales()
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);
            }
        })
    }

    const showInProgressPurchases = () => {
        let empty = true;
        $("#purchasesContent").html(purchasedItems.map(item => {
            if (item.Finished == 0) {
                empty = false;
                return `<div class="row justify-content-around align-items-center border border-secondary rounded mt-2 mb-2 p-2"> <div class="col col-6 col-md-3"><img class="productImage rt" src="../images/${item.Image}" alt="${item.ProductName}"></img></div> <div class="col col-6 col-md-5"> <div class="row"><b>#` + item.OrderId + '</b></div> <div class="row">' + item.ProductName + '</div>' + 
                '<div class="row">Sold by ' + item.Seller + '</div> </div> <div class="col col-6 col-md-3"><div class="row">' + item.Price + '€ </div> <div class="row">Quantity: ' + item.Quantity + 
                '</div><div class="row">Status: ' + item.Status + '</div> </div>'
                + '<div class="col col-6 col-md-3"><svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-truck" viewBox="0 0 16 16">' +
                '<path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>' +
                '</svg> </div></div>'
            }
        }));
        if (empty == true) $("#purchasesContent").html(noPrdocts)
    }

    

    const showFinishedPurchases = () => {
        let empty = true;
        $("#purchasesContent").html(purchasedItems.map(item => {
            if (item.Finished == 1) {
                empty = false;
                return `<div class="row justify-content-around align-items-center border border-secondary rounded mt-2 mb-2 p-2"> <div class="col col-6 col-md-3"><img class="productImage rt" src="../images/${item.Image}" alt="${item.ProductName}"></img></div> <div class="col col-6 col-md-5"> <div class="row"><b>#` + item.OrderId + '</b></div> <div class="row">' + item.ProductName + '</div>' + 
                '<div class="row">Sold by ' + item.Seller + '</div> </div> <div class="col col-6 col-md-3"><div class="row">' + item.Price + '€ </div> <div class="row">Quantity: ' + item.Quantity + 
                '</div><div class="row">Status: ' + item.Status + '</div> </div>'
                + '<div class="col col-6 col-md-3"><svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-check-circle" viewBox="0 0 16 16">' +
                '<path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>' +
                '<path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>' +
                '</svg> </div></div>'
            }
        }));
        if (empty == true) $("#purchasesContent").html(noPrdocts)
    }

    const showInProgressSales = () => {
        let empty = true;
        $("#salesContent").html(soldItems.map(item => {
            if (item.Finished == 0) {
                empty = false;
                return `<div class="row justify-content-around align-items-center border border-secondary rounded mt-2 mb-2 p-2"><div class="col col-6 col-md-3"><img class="productImage rt" src="../images/${item.Image}" alt="${item.Name}"></img></div><div class="col col-6 col-md-4"> <div class="row"><b>#${item.Id}</b></div><div class="row">${item.Name}</div>
                <div class="row">Sold to ${item.Customer}</div> </div> <div class="col col-12 col-md-5"> <div class="row">${item.Price}€ </div> <div class="row">Quantity:${item.Quantity}</div> <div class="row">Status:${item.Status}</div> </div></div>`;
            }
        }));
        if (empty == true) $("#salesContent").html(noPrdocts)
    }

    const showFinishedSales = () => {
        let empty = true;
        $("#salesContent").html(soldItems.map(item => {
            if (item.Finished == 1) {
                empty = false;
                return `<div class="row justify-content-around align-items-center border border-secondary rounded mt-2 mb-2 p-2"> <div class="col col-6 col-md-3"><img class="productImage rt" src="../images/${item.Image}" alt="${item.Name}"></img></div> <div class="col col-6 col-md-5"> <div class="row"><b>#` + item.Id + '</b></div> <div class="row">' + item.Name + '</div>' + 
                '<div class="row">Sold to ' + item.Customer + '</div> </div> <div class="col col-6 col-md-3"><div class="row">' + item.Price + '€ </div> <div class="row">Quantity: ' + item.Quantity + 
                '</div><div class="row">Status: ' + item.Status + '</div> </div>'
                + '<div class="col col-6 col-md-3"><svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-truck" viewBox="0 0 16 16">' +
                '<path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>' +
                '</svg> </div></div>'
            }
        }));
        if (empty == true) $("#salesContent").html(noPrdocts)
    }

    if(isLogged()) {
        $("#name").html(getUser().name)
        getOrders()
        getSoldItems()
        $("#showInProgressPurchases").click(() => showInProgressPurchases())
        $("#showFinishedPurchases").click(() => showFinishedPurchases())
        $("#showOnSale").click(() => showInProgressSales())
        $("#showFinishedSold").click(() => showFinishedSales())
    }
    else {
        window.location.replace("login.html")
    }
});