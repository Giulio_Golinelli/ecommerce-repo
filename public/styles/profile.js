$(function() {
    //every first child of a switch class have the btn-secondary class
    $(".switch button:first-child").removeClass("btn-light").addClass("btn-secondary");
    
    //switch selected buttons
    $('.switch').click(function (eventData) {
        if(!$(eventData.target).hasClass("btn-secondary"))
            $(this).children().each(function (){
                if($(this).hasClass("btn-light"))
                    $(this).removeClass("btn-light").addClass("btn-secondary");
                else
                $(this).removeClass("btn-secondary").addClass("btn-light");
            })
    });
});