<?php
include "cors.php";
include "utils.php";

$token = validateToken();
$conn = openConnection();
$name = testInput($_POST['name']);
$description = testInput($_POST['description']);
$price = testInput($_POST['price']);
$quantity = testInput($_POST['quantity']);
$userId = getUserId($token);
$fileName = $_FILES['image']['name'];
$array = explode(".", $fileName);
$ext = end($array);
echo $ext;
if ($ext == 'png' || $ext == 'jpg' || $ext == "" || $ext == 'jpeg') {
    move_uploaded_file($_FILES['image']['tmp_name'], '/var/www/html/public/images/' . $fileName);
    $query = "INSERT INTO Product(Name, Description, Price, Quantity, Seller, Image) VALUES(?,?,?,?,?,?)";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('ssdiis', $name, $description, $price, $quantity, $userId, $fileName);
    $stmt->execute();
    $conn->close();
    http_response_code(200);
} else {
    echo 'Invalid file format';
    http_response_code(403);
}

?>