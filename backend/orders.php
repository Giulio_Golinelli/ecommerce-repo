<?php
include "cors.php";
include "utils.php";
$token = validateToken();
$conn = openConnection();
$query = "SELECT * FROM User WHERE User.Id=(SELECT UserId FROM Token WHERE Token LIKE ?)";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$token);
$stmt->execute();
$result = $stmt->get_result();
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $products = array();
    $userId = $row["Id"];
    $query = "SELECT Purchase.Quantity, User.Name as Seller, Purchase.Price, Purchase.Timestamp, Purchase.Product, Purchase.Id, Product.Name, Purchase.Finished, Purchase.Status, Product.Image FROM Purchase, Product, User 
                WHERE Customer='$userId' AND Product.Id = Purchase.Product AND User.Id = Product.Seller ORDER BY Purchase.Timestamp DESC";
    $result = $conn->query($query);
    $products = array();
    while ($row = $result->fetch_assoc()) {
        $product = array();
        $product["OrderId"] = $row["Id"];
        $product["Price"] = $row["Price"];
        $product["Quantity"] = $row["Quantity"];
        $product["Timestamp"] = strtotime($row["Timestamp"]);
        $product["ProductName"] = $row["Name"];
        $product["Finished"] = $row["Finished"];
        $product["Status"] = $row["Status"];
        $product["Seller"] = $row["Seller"];
        $product["Image"] = $row["Image"];
        array_push($products, $product);
    }
    echo json_encode($products);
    http_response_code(200);
} ?>