<?php
include "cors.php";
include "utils.php";
$conn = openConnection();
$result = $conn->query("SELECT Product.Id, Product.Name, Product.Description, Product.Price, Product.Quantity, Product.DateAdded, Product.Image, User.Id as SellerId, User.Name as UserName FROM Product, User WHERE Quantity > 0 and User.Id = Product.Seller");
$conn->close();
$products = array();
if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        $product = array();
        $product["Id"] = $row["Id"];
        $product["Name"] = $row["Name"];
        $product["Description"] = $row["Description"];
        $product["Price"] = $row["Price"];
        $product["Quantity"] = $row["Quantity"];
        $product["DateAdded"] = $row["DateAdded"];
        $product["Seller"] = $row["SellerId"];
        $product["SellerName"] = $row["UserName"];
        $product["Image"] = $row["Image"];
        array_push($products, $product);
    }
}
echo json_encode($products);
http_response_code(200);
?>
