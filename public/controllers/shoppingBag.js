// Functions and structures for shopping bag management

//set a new user as the current logged in user
export const setShoppingBag = (shoppingBag) => localStorage.setItem("shoppingBag", JSON.stringify(shoppingBag))

export const insertTo = (product, quantity) => {
    const shoppingBag = getShoppingBag()
    if (shoppingBag.length == 0 || shoppingBag.every(o => o.product.Id !== product.Id))
        shoppingBag.push({
            product: product,
            quantity: quantity
        })
    else {
        let order = shoppingBag.find(o => o.product.Id == product.Id)
        order.quantity = +order.quantity + +quantity;
    }
    setShoppingBag(shoppingBag)
}

export const deleteFrom = (productId) => {
    const shoppingBag = getShoppingBag()
    const newShoppingBag = [] 
    shoppingBag.forEach((item) => { if (item.product.Id != productId) newShoppingBag.push(item) })
    setShoppingBag(newShoppingBag)
}

// Get the current logged user
export const getShoppingBag = () => JSON.parse(localStorage.getItem("shoppingBag"))

//unset the current logged in user
export const eraseShoppingBag = () => localStorage.removeItem("shoppingBag")