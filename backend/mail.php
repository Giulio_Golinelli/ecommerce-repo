<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

function sendMail($receiver, $uuid){
// Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        ///Create a new PHPMailer instance
        $mail = new PHPMailer();

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = 'ecommerceunibo@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = 'Brixia2020';

        //Set who the message is to be sent from
        $mail->setFrom('ecommerceunibo@gmail.com', 'Ecommerce');

        //Set who the message is to be sent to
        $mail->addAddress($receiver, 'New User');

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Activate your account';
        $mail->Body = '<p>Click on the following link to confirm you account:</ br> http://37.187.122.91/backend/activation.php?uuid=' . $uuid . '</p>';
        $mail->AltBody = 'Click on the following link to confirm you account: http://37.187.122.91/backend/activation.php?uuid=' . $uuid;

        //send the message, check for errors
        if (!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message sent!';
        }
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}

?>