// Functions and structures for client user management

//check if there is a logged in account
export const isLogged = () => localStorage.user ? true : false

//set a new user as the current logged in user
export const setUser = (user) => localStorage.setItem("user", JSON.stringify(JSON.parse(user)))

// Get the current logged user
export const getUser = () => JSON.parse(localStorage.getItem("user"))

//unset the current logged in user
export const eraseUser = () => localStorage.removeItem("user")