CREATE PROCEDURE update_status()
BEGIN
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE OrderStatus ENUM ('accepted','in transit','arrived');
    DECLARE PurchaseId, CustomerId INT;
    DECLARE cur CURSOR FOR SELECT Id, Status, Customer FROM Purchase;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
    read_loop:
    LOOP
        FETCH cur INTO PurchaseId, OrderStatus, CustomerId;
        IF done THEN
            LEAVE read_loop;
        END IF;
        IF OrderStatus = 'accepted' THEN
            UPDATE Purchase SET Status='in transit' WHERE Id = PurchaseId;
            INSERT INTO Messages(Name, UserId) VALUES (CONCAT('Order ', PurchaseId, 'is in transit!'), CustomerId);
        ELSEIF OrderStatus = 'in transit' THEN
            UPDATE Purchase SET Status='arrived' WHERE Id = PurchaseId;
            INSERT INTO Messages(Name, UserId) VALUES (CONCAT('Order ', PurchaseId, 'is arrived!'), CustomerId);
        ELSEIF OrderStatus = 'arrived' THEN
            UPDATE Purchase SET finished= 1 WHERE Id = PurchaseId;
            INSERT INTO Messages(Name, UserId) VALUES (CONCAT('Order ', PurchaseId, 'is finished!'), CustomerId);
        END IF;
    END LOOP;

    CLOSE cur;
end;

CREATE EVENT status_change_event
    ON SCHEDULE EVERY 5 MINUTE
    DO
    BEGIN
        CALL update_status();
    end;