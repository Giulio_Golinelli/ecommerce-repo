<?php
include "cors.php";
include "utils.php";

$token = validateToken();
$conn = openConnection();
$query = "SELECT * FROM User WHERE User.Id=(SELECT UserId FROM Token WHERE Token LIKE ?)";
$stmt = $conn->prepare($query);
$stmt->bind_param('s',$token);
$stmt->execute();
if ($result = $stmt->get_result())
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $account = array();
        $account["userId"] = $row["Id"];
        $account["name"] = $row["Name"];
        $account["surname"] = $row["Surname"];
        $account["email"] = $row["Email"];
        echo json_encode($account);
        http_response_code(200);
        return;
    } else
        http_response_code(403);
else {
    echo "Query Failed!";
    exit();
}
?>